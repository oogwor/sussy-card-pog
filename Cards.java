import java.util.ArrayList;
import java.util.Collections;

public class Cards{
    private String[] ranks = {"A", "2", "3", "4", "5", "6", "7", "8", "8", "10", "J", "Q", "K"};
    private String[] suits = {"C", "D", "S", "H"};
    private ArrayList<String> deck = new ArrayList<String>();

    /*
    Initializes a Cards object
     */ 
    public Cards(){
        for (String suit: suits) {
            for(String rank: ranks){
                deck.add(suit+rank);
            }
        }
    }

    /*
     * Shuffles the deck
     */
    public void shuffle(){
        Collections.shuffle(deck);
    }

    /*
     * @return The deck
     */
    public ArrayList<String> getDeckList(){
        return deck;
    }

    /*
     * @return A random card in the deck
     */
    public String getRandomCard(){
        return deck.get((int)(Math.random() * deck.size()));
    }

    /*
     * @param pos The position of the card
     * @return you know
     */
    public String getCard(int pos){
        if(pos <= deck.size()){
            return deck.get(pos);
        }
        return null;
    }


}